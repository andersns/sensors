#!/usr/bin/perl
#
# TODO:	- Add retries to reading files.
#	- Could one automate %sensors?
#	- Read triggered alarms from sensors.
#	- Write more sensor info to spool files
#	- Lockfile
#
use strict;
use warnings;
use Carp;
use Getopt::Long;
use File::Path;
use File::Copy;

my @files      = ();
my $owfs       = '/mnt/1wire';
my $find       = '/usr/bin/find';
my $fam_id     = '??.????????????';
my $cached     = 0;
my $find_path  = '';
my $spooldir   = '/var/spool/sensor';
my $verbose    = 0;

my %sensors    = (
	'DS18B20'	=> { 'temperature' => 'temperature' },
	'EDS'		=> { 'humidity' => 'EDS0065/humidity', 'temperature' => 'EDS0065/temperature'}
);


GetOptions (
	"verbose!" => \$verbose,
	"cached!"  => \$cached,
);

#
# Pre-run checks
#
if ( ! -d $spooldir ) {
	print "Creating missing spool folder $spooldir.\n" if $verbose;
	mkpath( $spooldir );
}

#
# Search for sensors
#
if ( ! $cached ) {
	$find_path = "$owfs/uncached/$fam_id";
	print "Using uncached datastructure ($find_path)\n" if $verbose;
} else {
	$find_path = "$owfs/$fam_id";
	print "Using cached datastructure ($find_path)\n" if $verbose;
}

@files = `$find $find_path -type f -name type`;


#
# Process each sensor found.
#
foreach my $file ( @files ) {
	chomp($file);
	print "Checking sensor $file ..\n" if $verbose;
	my %sensordata;
	$sensordata{ dir }     = substr( $file, 0, -5);
	$sensordata{ id }      = get_value_from_file( $sensordata{ dir }, 'id'      );
	$sensordata{ family }  = get_value_from_file( $sensordata{ dir }, 'family'  );
	$sensordata{ address } = get_value_from_file( $sensordata{ dir }, 'address' );
	$sensordata{ type }    = get_value_from_file( $sensordata{ dir }, 'type'    );

	if ( defined( $sensors{ $sensordata{ type } } ) ) {
		print "\tFound definitions for sensor type $sensordata{ type }!\n" if $verbose;
		foreach my $samplename ( sort keys $sensors{ $sensordata{ type } } ) {
			print "\tChecking \'$samplename\' from $sensordata{ family}.$sensordata{ id }\n" if $verbose;
			my $samplefile  = $sensors{ $sensordata{ type } }{ $samplename };
			my $samplevalue = get_value_from_file( $sensordata{ dir }, $samplefile );
			my $sampletime  = time;
			$samplevalue    =~ s/\s//g;	# Remove junk whitespace from value.
			print "\tGot $samplename with value $samplevalue at epoch $sampletime.\n" if $verbose;
			my $filename    = "$spooldir/$sampletime-" . gen_random_string() . ".dat";

			my $content;
			$content .= "sensoraddress=${sensordata{ address }}\n";
			$content .= "sensortype=${sensordata{ type }}\n";
			$content .= "samplename=${samplename}\n";
			$content .= "sampletime=${sampletime}\n";
			$content .= "samplevalue=$samplevalue\n";

			print "\tWriting sample data to $filename\n" if $verbose;
			unless( write_value_to_file( $filename, $content ) ) {
				carp( "WARNING: Failed to write value sample data to $filename." );
			}
		}
	} else {
		carp( "WARNING: Unknown sensor type ${sensordata{ type }}, skipping data collection for ${sensordata{ dir }}." );
	}
}



################################# SUBS #################################

sub gen_random_string {
	my @set = ('0' ..'9', 'A' .. 'F');
	my $str = join '' => map $set[rand @set], 1 .. 8;

	return $str;
}

sub write_value_to_file {
	my $file    = shift;
	my $content = shift;
	my $status  = 0;
	my $tmp     = "$file.tmp";

	if ( -e $file ) {
		carp( "WARNING: Output file $file already exists, skipping." );
	} else {
		if ( open(FD, ">$tmp") ) {
			print FD $content;
			$status = 1 if ( close(FD) );
			print "\tWrote to $tmp, moving to $file\n" if $verbose;
			$status = 0 unless ( move( $tmp, $file) );
		} else {
			carp( "WARNING: unable to open file $file for writing." );
		}
	}

	return $status;
}

sub get_value_from_file {
	my $d = shift;
	my $f = shift;
	my $v = undef;
	
	if ( open(FD, "$d/$f") ) {
		$v = <FD>;
		close( FD );
	} else {
		carp( "WARNING: Unable to retrive value from $d/$f." );
	}

	return $v;
}


__END__
