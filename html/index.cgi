#!/usr/bin/perl
use strict;
use warnings;
use DBI;
use Config::Tiny;
use CGI;
use JSON;
use Data::Dumper;

my %sensors;
my %locations;
my %structure;
my %children;
my $config;

# Read config file
my $file    = "/opt/sensors/sensor.conf";
$config     = Config::Tiny->new;
$config     = Config::Tiny->read( $file );
my $db_user = $config->{ database }->{ username };
my $db_pass = $config->{ database }->{ password };
my $db_name = $config->{ database }->{ database };

# Connect to database
my $dbh     = DBI->connect("dbi:mysql:" . $db_name,$db_user,$db_pass);

# Populate data structures
populate_locations( $dbh );
populate_sensors( $dbh );

my $q      = CGI->new;
my $id     = $q->param('id') || 0;
my $period = $q->param('period') || 3600;

my @data   = ();
@data = get_data( $dbh, $id, $period ) if ( $id );

print "Content-type: text/html\n\n";
print_header( \@data );

print "    <div id='sensortree'>\n";
print_structure( \%structure, "      " );
print "    </div>\n";

print "    <div id='graphwindow'>\n";
if ( $id ) {
	print "      <div id='container' style='min-width: 400px; height: 400px; margin: 0 auto'></div>\n";
	print "      <div id='period'>[<a href=\'index.cgi?id=$id&period=3600\'>1 hour</a>] [<a href=\'index.cgi?id=$id&period=86400\'>1 day</a>] [<a href=\'index.cgi?id=$id&period=604800\'>7 days</a>] [<a href=\'index.cgi?id=$id&period=2592000\'>30 days</a>]</div>\n";
} else {
	print "      Please select sensor to graph.\n";
}
print "    </div>\n";

print_footer();

$dbh->disconnect;

exit 0;


################### SUBS ###################

sub get_data {
	my $dbh       = shift;
	my $sensorid  = shift;
	my $interval  = shift;
	my $time_to   = time;
	my $time_from = $time_to - $interval;
	my @sensordata;

	my $sql = qq{SELECT time, value FROM samples WHERE sensor=? AND time BETWEEN ? AND ? ORDER BY time };
	my $sth = $dbh->prepare($sql);
	my $rv  = $sth->execute( $sensorid, $time_from, $time_to );

	while (my $results = $sth->fetchrow_hashref) {
		push( @sensordata, [ $results->{ time } * 1000 , $results->{ value } + 0 ] );
	}

	return @sensordata;
}

# Get sensors for location
sub get_sensors_from_location {
	my $l = shift;
	my @s = ();

	foreach my $k ( sort keys %sensors ) {
		push( @s, $k ) if ( $sensors{ $k }{ location } == $l );
	}

	return @s;
}

# Print sensor structure
sub print_structure {
	my $r = shift;
	my $i = shift;

	print "$i<ul>\n";
	foreach my $k ( reverse sort keys %{$r} ) {
		my $name = $locations{ $k }{ name };
		my @s    = get_sensors_from_location( $k );
		if ( keys %{ $r->{ $k } } ) { 
			print "$i  <li id='location'>\n";
			print "$i    $name\n";
			if ( @s ) {
				print "$i    <ul>";
				foreach my $sensor ( @s ) { print "<li id='sensor'>" . $sensors{ $sensor }{ unit } . " (" . $sensors{ $sensor }{ address } . ")</li>\n" }
				print "</ul>\n";
			}
			print_structure( $r->{ $k }, "$i    " ) if (keys %{ $r->{ $k } });
			print "$i  </li>\n";
		} else {
			print "$i  <li id='location'>$name\n";
			if ( @s ) {
				print "$i    <ul>";
				foreach my $sensor ( @s ) { print "<li id='sensor'><a href='index.cgi?id=$sensor'>" . $sensors{ $sensor }{ address } . "</a> (" . $sensors{ $sensor }{ unit } . ")</li>\n" }
				print "</ul>\n";
			}
			print "$i  </li>\n";
		}
	}
	print "$i</ul>\n";
}

# Build sensor hash from database
sub populate_sensors {
	my $sql = "SELECT * FROM sensor";
	my $sth = $dbh->prepare($sql);
	my $rv  = $sth->execute;

	while (my $results = $sth->fetchrow_hashref) {
		my $id = $results->{ id  };
		$sensors{ $id }{ address }  = $results->{ address }  || 'unknown';
		$sensors{ $id }{ type }     = $results->{ type }     || 'unknown';
		$sensors{ $id }{ unit }     = $results->{ unit }     || 'unknown';
		$sensors{ $id }{ location } = $results->{ location } || 1;
	}
}

# Build locations and structure has from database
sub populate_locations {
	my $dbh2 = shift;

	my $sql  = "SELECT * FROM location";
	my $sth  = $dbh->prepare($sql);
	my $rv   = $sth->execute;

	while (my $results = $sth->fetchrow_hashref) {
	        my $id                      = $results->{ id };
	        my $name                    = $results->{ name };
	        my $parentid                = $results->{ parent };
		$locations{ $id }{ name }   = $name;
		$locations{ $id }{ parent } = $parentid;
	
		my $parent;
		if ( $parentid != 0 ) {
			$parent = $children{$parentid} ||= {};	
		} else {
			$parent = \%structure;
		}

        	$parent->{$id} = $children{$id} ||= {};
	}
}

sub print_header {
	my $dataref = shift;
	print "<html>\n";
	print "  <head>\n";
	print "    <title>Sensor data</title>\n";
        print "    <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js'></script>\n";
	print "    <script src='http://code.highcharts.com/highcharts.js'></script>\n";
	print "    <script src='http://code.highcharts.com/modules/exporting.js'></script>\n";
#	print "    <script src='js/1w.js'></script>\n";
	print "    <link href='sensor.css' rel='stylesheet' type='text/css'>\n";
        print "    <link rel='stylesheet' type='text/css' href='http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css' media='screen' />\n";
	print_jsscript( $dataref );
	print "  </head>\n";
	print "  <body>\n";
        print "    <div id='topmenu' width='100%' align='center'><a href=''>Manage locations</a></div>\n";
}

sub print_footer {
	print "  </body>\n";
	print "<html>\n";
}

sub print_jsscript {
	my $dataref  = shift;
	my $address = $sensors{ $id }{ address };
	my $unit    = $sensors{ $id }{ unit };

	my $json = $dataref;
	my $json_text = to_json($json);

	print "<script>						\n";
	print "  var chart;					\n";
	print "  \$(document).ready(function() {		\n";
	print "    chart = new Highcharts.Chart({		\n";
	print "      chart: {				\n";
	print "        renderTo: 'container',		\n";
	print "        type: 'line',			\n";
	print "        zoomType: 'xy'			\n";
	print "      },					\n";
	print "      plotOptions: {			\n";
	print "        line: {				\n";
	print "          connectNulls: true,		\n";
	print "          marker:{			\n";
	print "            enabled: false,		\n";
	print "          }				\n";
	print "        }				\n";
	print "      },					\n";
	print "      title: {				\n";
	print "        text: \'$address\',		\n";
	print "        x: -20 //center			\n";
	print "      },					\n";
	print "      xAxis: {				\n";
	print "        tickmarkPlacement: 'on',		\n";
	print "        gridLineWidth: 1,		\n";
	print "        type: 'datetime'			\n";
	print "      },					\n";
	print "      yAxis: {				\n";
	print "        labels: this.value,		\n";
	print "        type: 'linear',			\n";
	print "        title: { text: \'$unit\' },	\n";
	print "        plotLines: [{			\n";
	print "          value: 0,			\n";
	print "          width: 1,			\n";
	print "          color: '#808080'		\n";
	print "        }]				\n";
	print "      },					\n";
	print "      tooltip: {				\n";
	print "        formatter: function() {		\n";
	print "          return '<b>'+ this.series.name +'</b><br/>'+\n";
	print "          \'$unit: \'+ Highcharts.numberFormat(this.y, 2) + '<br/>' + \n";
	print "          'Date: ' + Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x)\n";
	print "        }\n";
	print "      },\n";
	print "      series: [{\n";
	print "        name: \'$address\',\n";
	print "        data: $json_text\n";
	print "      }]\n";
	print "    });\n";
	print "  });\n";
	print "</script>\n";
}
