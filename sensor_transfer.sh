#!/bin/sh

DST_USER="sensor"
DST_HOST="pluto.darkdeep.net"
DST_PATH="/var/spool/sensor/"

/usr/bin/rsync -avz --remove-source-files -e ssh /var/spool/sensor/*.dat $DST_USER@$DST_HOST:$DST_PATH
