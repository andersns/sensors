#!/usr/bin/perl
#
# TODO: - Add lock file to script, to avoid multiple runs.
#       - Clean up db-code.
#       - Improve code for reading the files.
#	- Improve error handling.
#	- Make script use config file for settings.
# 
use strict;
use DBI;
use warnings;
use Getopt::Long;

my $verbose        = 0;
my $spooldir       = '/var/spool/sensor';
my $unknown_loc_id = 1;

GetOptions (
        "verbose!" => \$verbose,
);

# Search for samplefiles.
my @samplefiles = `/usr/bin/find $spooldir -type f`;

if ( @samplefiles == 0 ) {
	print "No sample files. Exiting.\n" if $verbose;
	exit 0;
}

# Connect to database.
my $dbh = DBI->connect("dbi:mysql:sensor","sensor","vooSita6")
		or die "I cannot connect to dbi:mysql:sensor as sensor - $DBI::errstr\n";

# Populate sensor cache from database.
my %sensors;
print "Populate sensors from database ..\n" if $verbose;
my $sql  = "SELECT * FROM sensor";
my $sth  = $dbh->prepare($sql);
my $rv   = $sth->execute;

while (my $results = $sth->fetchrow_hashref) {
	my $sensorid = $results->{ id };
	my $sensoraddress = $results->{ address };
	my $sensorunit = $results->{ unit };

	$sensors{ $sensoraddress }{ $sensorunit }{ id } = $sensorid;

	print "\tadded sensoraddress=$sensoraddress sensorunit=$sensorunit sensorid=$sensorid\n" if $verbose;
}

# Get sampledata from files
print "Processing sample files ..\n" if $verbose;
foreach my $samplefile ( @samplefiles ) {
	chomp( $samplefile );
	if ( open(FD, $samplefile) ) {
		my %sampledata;

		while(<FD>) {
			chomp();
			my ($key,$value) = split("=", $_);
			$sampledata{ $key } = $value;
		}
		close(FD);

		my $sensortype    = $sampledata{ sensortype    };
		my $sensoraddress = $sampledata{ sensoraddress };
		my $sampletime    = $sampledata{ sampletime    };
		my $samplename    = $sampledata{ samplename    };
		my $samplevalue   = $sampledata{ samplevalue   };

		print "\tGot sensordata from $samplefile: type=$sensortype address=$sensoraddress time=$sampletime name=$samplename value=$samplevalue\n" if $verbose;

		if ( ! defined( $sensors{$sensoraddress}{$samplename} ) ) {
			print "\t\tINFO: Sensor not found in datastructure, adding to database with UNKNOWN location.\n" if $verbose;

			my $sql2 = qq{INSERT INTO sensor VALUES ( NULL, ?, ?, ?, ? )};
			my $sth2 = $dbh->prepare( $sql2 );
			my $rv2  = $sth2->execute( $sensoraddress, $sensortype, $samplename, $unknown_loc_id );

			my $sql3 = qq{SELECT id FROM sensor WHERE address=? AND unit=?};
			my $sth3 = $dbh->prepare( $sql3 );
			my $rv3  = $sth3->execute( $sensoraddress, $samplename );

			while (my $results = $sth3->fetchrow_hashref) {
				my $sid      = $results->{ id };
				$sensors{ $sensoraddress }{ $samplename }{ id } = $sid;
				print "\t\tSensor $sensoraddress with unit $samplename added with sensorid=$sid.\n" if $verbose;
			}
		} 

		print "\tInserting sample into database .. " if $verbose;
		my $sql4 = qq{INSERT INTO samples VALUES ( NULL, ?, ?, ?)};
		my $sth4 = $dbh->prepare( $sql4 );
		my $rv4  = $sth4->execute( $sensors{ $sensoraddress }{ $samplename }{ id }, $samplevalue, $sampletime );

		if ( $rv4 ) {
			print "success.\n" if $verbose;
			unlink( $samplefile ) or warn "WARNING: Could not unlink $samplefile: $!";
		} else {
			print "failed.\n" if $verbose;
			# Since insert failed, we do not delete sample file!
		}
	}
}

$dbh->disconnect;
